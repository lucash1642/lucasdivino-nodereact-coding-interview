import React, { FC, useEffect, useState } from "react";

import { Button, CircularProgress } from "@mui/material";
import { RouteComponentProps } from "@reach/router";
import { UserCard } from "../components/users/user-card";
import { IUserProps } from "../dtos/user.dto";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

const PAGE_SIZE = 10;

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState(false);

  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(1);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);

      const result = await backendClient.getAllUsers(page, PAGE_SIZE);

      setUsers(result.data);
      setTotal(result.total);

      setLoading(false);
    };

    fetchData();
  }, [page]);

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            <div>
              {users.length
                ? users.map((user) => {
                    return <UserCard key={user.id} {...user} />;
                  })
                : null}
            </div>
            <div>
              <span>
                <Button
                  onClick={() => setPage((prev) => Math.max(1, prev - 1))}
                >
                  PREV
                </Button>
              </span>
              <span>Page {page}</span>
              <span>
                <Button
                  onClick={() => setPage((prev) => Math.min(prev + 1, total))}
                >
                  PREV
                </Button>
              </span>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

import people_data from "../data/people_data.json";

export class PeopleProcessing {
  getById(id: number) {
    return people_data.find((p) => p.id === id);
  }

  getAll(currentPage: number, limit: number) {
    const response = {
      data: people_data.slice(currentPage * limit, (currentPage + 1) * limit),
      total: Math.floor(people_data.length / limit),
    };
    return response;
  }
}

import {
  Get,
  HttpCode,
  JsonController,
  NotFoundError,
  Param,
  QueryParams,
} from "routing-controllers";
import { PeopleProcessing } from "../services/people_processing.service";

const peopleProcessing = new PeopleProcessing();

@JsonController("/people", { transformResponse: false })
export default class PeopleController {
  @HttpCode(200)
  @Get("/")
  getAllPeople(@QueryParams() params: { currentPage: number; limit: number }) {
    const currentPage = params.currentPage ? Number(params.currentPage) : 1;
    const limit = params.limit ? Number(params.limit) : 10;

    const people = peopleProcessing.getAll(currentPage, limit);

    if (!people) {
      throw new NotFoundError("No people found");
    }

    return people;
  }

  @HttpCode(200)
  @Get("/:id")
  getPerson(@Param("id") id: number) {
    const person = peopleProcessing.getById(id);

    if (!person) {
      throw new NotFoundError("No person found");
    }

    return {
      data: person,
    };
  }
}
